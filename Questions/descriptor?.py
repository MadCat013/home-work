#2-13 - после того как точка создана ей можно поменять координаты. нужно обеспечить, чтобы координаты были только числами.
#для линии то же смое.
#используй проперти или дескриптор

class OnlyPointDescriptor:

    def __set__(self, instance, value):
        if isinstance(value, Point):
            setattr(self, '_value', value)
        else:
            raise TypeError

    def __get__(self, instance, owner):
        return getattr(self, '_value')


class OnlyIntDescriptor:

    def __set__(self, instance, value):
        if isinstance(value, (int, float)):
            setattr(self, '_value', value)
        else:
            raise TypeError

    def __get__(self, instance, owner):
        return getattr(self, '_value')


class Point:
    x = OnlyIntDescriptor()
    y = OnlyIntDescriptor()

    def __init__(self, new_x, new_y):

        self.x = new_x
        self.y = new_y

    def __add__(self, other):
        return Line(Point(self.x, self.y), Point(other.x, other.y))


class Line:
    start_point = OnlyPointDescriptor()
    end_point = OnlyPointDescriptor()

    def __init__(self, begin_point, end_point):
        self.start_point = begin_point
        self.end_point = end_point

    def length_getter(self):
        res = ((self.start_point.x - self.end_point.x) ** 2 + (self.start_point.y - self.end_point.y) ** 2) ** 0.5
        return res

    length = property(length_getter)


new_line = Line(Point(7, 3), Point(9, 2))
print(f'{new_line.length} < -------------Длинна линии')


new_points = Point(5, 9)
new_points2 = Point(3, 5)
result = new_points + new_points2
print(result)
print(type(result))
print(f'{result.length} <-----------Длинна сложенной линии')

