from timeit import timeit
import time


def func_time_decorator(func):

    def _wrapper(*args, **kwargs):
        print(func)
        start_time = time.time()
        res = func(*args, **kwargs)
        end_time = time.time()
        print(res)
        clock5 = start_time - end_time
        return clock5
    return _wrapper


@func_time_decorator
def fib_recursive(numb):

    if numb in (1, 2):
        return 1

    return fib_recursive(numb - 1) + fib_recursive(numb - 2)


# clock1 = fib_recursive(33)
# print(clock1)
# clock2 = timeit(lambda: fib_recursive(33), number=1)
# print(clock2)

clock3 = fib_recursive(30)
print(clock3)
