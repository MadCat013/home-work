# В отдельном файле (пусть будет lib.py) написать функцию, которая требует от пользователя ответить да или нет (Y/N) и возвращает True/False в зависимости от того, что он ввел. В основном файле (пусть будет main_file.py) попросить пользователя ввести с клавиатуры строку и вывести ее на экран. Используя импортированную из lib.py функцию спросить у пользователя, хочет ли он повторить операцию (Y/N). Повторять пока пользователь отвечает Y и прекратить когда пользователь скажет N.
from lib import answer_function
from lib import year_case
from lib import type_age
from lib import advice

user_answer = True
while user_answer is True:
    user_input = input('Введитек строку :')
    print(user_input)
    user_answer = answer_function(input('Хотите попробовать ещё раз? Y/N :'))




# Модифицируем ДЗ2. Напишите с помощью функций!. Помните о Single Respinsibility! Попросить ввести свой возраст (можно использовать константу или input()). Пользователь ввел значение возраста [year number] а на место [year string] нужно поставить правильный падеж существительного "год", который зависит от значения [year number].
# - если пользователь ввел непонятные данные (ничего не ввел, ввел не число, неактуальный возраст и тд.) - вывести “не понимаю”
# - если пользователю меньше 7 - вывести “Тебе [year number] [year string], где твои мама и папа?”
# - если пользователю меньше 18 - вывести “Тебе [year number] [year string], а мы не продаем сигареты несовершеннолетним”
# - если пользователю больше 65 - вывести “Вам уже [year number] [year string], вы в зоне риска”!
# - в любом другом случае - вывести “Оденьте маску, вам же [year number] [year string]!”
# Например:

# Тебе 1 год, где твои мама и папа?

# Оденьте маску, вам же 23 года!

# Вам уже 68 лет, вы в зоне риска!


year_number = type_age(input('Введите возраст'))

year_string = year_case(year_number)

user_advice = advice(year_number, year_string)
print(user_advice)


# if int(year_number) < 7:
#     print(f'Тебе {year_number} {year_string} Где твои мама и папа?')
# elif int(year_number) < 18 and year_number:
#     print(f'Тебе {year_number} {year_string} , а мы не продаём сирареты несовершеннолетним!')
# elif 65 < int(year_number) < 150:
#     print(f'Вам уже {year_number} {year_string}, вы в зоне риска!')
# elif int(year_number) > 150:
#     print(f'Вам {year_number} {year_string}????, для вас советов нет... ')
# else:
#     print(f'Оденьте маску, вам же {year_number} {year_string}!')
