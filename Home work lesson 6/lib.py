def answer_function(answer):
    while answer != 'N' and answer != 'Y':
        try:
            answer = input('Введите "Y" или "N" :')
            continue
        finally:
            continue
    if answer == 'Y':
        return True
    elif answer == 'N':
        return False


def year_case(age):
    if age.endswith('0'):
        age = 'Лет'
    elif age.endswith('11') or age.endswith('12') or age.endswith('13') or age.endswith('14'):
        age = 'Лет'
    elif age.endswith('1'):
        age = 'Год'
    elif age.endswith('2') or age.endswith('3') or age.endswith('4'):
        age = 'Года'
    elif age.endswith('5') or age.endswith('6') or age.endswith('7') or age.endswith('8') or age.endswith(
            '9'):
        age = 'Лет'
    return age


def type_age(number):
    while type(number) != int:
        try:
            int(number)
            break
        except ValueError:
            print('Не понимаю!')
            number = input('Ведите свой возраст числом: ')
    return number


def advice(number, word):
    if int(number) < 7:
        advice_string = f'Тебе {number} {word} Где твои мама и папа?'
    elif int(number) < 18 and number:
        advice_string = f'Тебе {number} {word} , а мы не продаём сирареты несовершеннолетним!'
    elif 65 < int(number) < 150:
        advice_string = f'Вам уже {number} {word}, вы в зоне риска!'
    elif int(number) > 150:
        advice_string = f'Вам {number} {word}????, для вас советов нет... '
    else:
        advice_string = f'Оденьте маску, вам же {number} {word}!'
    return advice_string
