class Animal:
    paws = 0
    sound = ''
    name = ''

    def __init__(self, new_paws, new_sound, new_name):
        if 0 < int(new_paws) <= 4:
            self.paws = new_paws
        if type(new_sound) == str:
            self.sound = new_sound
        if len(new_name) < 20:
            self.name = new_name

    def animal_voice(self):
        return f' Hello my name is "{self.name}" i have "{self.paws}" paws my voice is "{self.sound}"'


dog = Animal(4, "Gav-Gav", "Rex")
cat = Animal(4, "Мяу-Мяу", "Kitty")
bird = Animal(2, "Чирик - чирик", "Кеша")

print(bird.animal_voice())
print(dog.animal_voice())
print(cat.animal_voice())
