import time


def func_time_decorator(func):

    def _wrapper(*args, **kwargs):
        start_time = time.time()
        func(*args, **kwargs)
        end_time = time.time()
        clock = end_time - start_time
        print(f'the time for "function" in WRAPPER is {clock}')
        return clock
    return _wrapper


@func_time_decorator
def counter1():
    counter = 15000
    my_list = list()
    while counter > 1:
        counter -= 1
        for i in range(200):
            res = i ** 100 / 3
            my_list.append(res)
    return counter


print(f' Tome for function "Counter" here ---->>> {counter1()}')


@func_time_decorator
def my_foo():
    my_dict = {}
    counter = 2000
    while counter > 5:
        counter -= 1
        for i in range(2000):
            res = i ** 5
            my_dict[i] = res
    return my_dict


print(f' Tome for function "my_foo" here ---->>> {my_foo()}')

