import time
from time import strftime
from time import gmtime


def check_clock(arg_type=None):

    def _wrapper_outer(func):

        def _wrapper_inner(*f_args, **f_kwargs):
            start_time = time.time()
            res = f'Function result - >>>{func(*f_args, **f_kwargs)}'
            end_time = time.time()
            clock = end_time - start_time
            if arg_type == "sec":
                print(f'Function time is {clock} seconds')
                return res, strftime("%H:%M:%S", gmtime(clock))
            elif arg_type == "min":
                print(f'Function time is {clock / 60} minutes')
                return res, strftime("%H:%M:%S", gmtime(clock))
            elif arg_type == "hour":
                print(f'Function time is {(clock / 60) / 60} Hour')
                return res, strftime("%H:%M:%S", gmtime(clock))
            else:
                return res, strftime("%H:%M:%S", gmtime(clock))

        return _wrapper_inner

    return _wrapper_outer


@check_clock("sec")
def counter1():
    counter = 45000
    my_list = list()
    while counter > 1:
        counter -= 1
        for i in range(200):
            res = i ** 100 / 3
            my_list.append(res)
    return counter


print(f' Time for function "Counter" in "SECONDS" here ---->>> {counter1()}')


@check_clock("min")
def counter2():
    counter = 90000
    my_list = list()
    while counter > 1:
        counter -= 1
        for i in range(200):
            res = i ** 100 / 3
            my_list.append(res)
    return counter


print(f' Time for function "Counter" in "MINUTES" here ---->>> {counter2()}')


@check_clock("hour")
def counter3():
    counter = 200000
    my_list = list()
    while counter > 1:
        counter -= 1
        for i in range(200):
            res = i ** 100 / 3
            my_list.append(res)
    return counter


print(f' Time for function "Counter" in "HOURS" here ---->>> {counter3()}')


@check_clock()
def counter4():
    counter = 220000
    my_list = list()
    while counter > 1:
        counter -= 1
        for i in range(200):
            res = i ** 100 / 3
            my_list.append(res)
    return counter


print(f' Time for function "Counter" without TIME parameter here ---->>> {counter4()}')
