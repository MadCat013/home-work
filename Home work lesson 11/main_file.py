
class Point:
    x = 0
    y = 0

    def __init__(self, new_x, new_y):
        if not isinstance(new_x, (int, float)) or not isinstance(new_y, (int, float)):
            raise TypeError
        self.x = new_x
        self.y = new_y

    def __add__(self, other):
        return Line(Point(self.x, self.y), Point(other.x, other.y))


class Line:
    start_point = None
    end_point = None

    def __init__(self, begin_point, end_point):
        if not isinstance(begin_point, Point) or not isinstance(end_point, Point):
            raise TypeError
        self.start_point = begin_point
        self.end_point = end_point

    def length_getter(self):
        res = ((self.start_point.x - self.end_point.x) ** 2 + (self.start_point.y - self.end_point.y) ** 2) ** 0.5
        return res

    length = property(length_getter)


new_line = Line(Point(7, 3), Point(9, 2))
print(f'{new_line.length} < -------------Длинна линии')


new_points = Point(5, 9)
new_points2 = Point(3, 5)
result = new_points + new_points2
print(result)
print(type(result))
print(f'{result.length} <-----------Длинна сложенной линии')
