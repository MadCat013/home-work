import json
import requests
import pickle
date = 20210619
my_url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json&date={date}'


def create_dict(url: str):
    my_dict = {}
    try:
        req = requests.get(url)
    except:
        return "some exception"
    else:
        print("all good")
        if 300 > req.status_code >= 200 and req.headers['Content-Type'] == 'application/json; charset=utf-8':
            req_json = req.json()
            print(req_json)
            for dic in req_json:
                for key in dic:
                    if key == 'txt':
                        my_dict[dic["txt"]] = dic["rate"]

            return my_dict


new_dict = create_dict(my_url)
print(new_dict)
with open('data_currency.txt', 'a') as file:
    file.write(str(new_dict))
