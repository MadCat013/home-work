import json
import requests
import pickle
date = 20210619
my_url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json&date={date}'


def create_dict(url: str):
    my_dict = {}
    try:
        req = requests.get(url)
    except:
        return "some exception"
    else:
        print("all good")
        print(req.status_code)
        print(req.headers)
        if 300 > req.status_code >= 200 and req.headers['Content-Type'] == 'application/json; charset=utf-8':
            req_json = req.json()
            print(req_json)
            for dic in req_json:
                for key in dic:
                    if key == 'txt':
                        my_dict[dic["txt"]] = dic["rate"]

            return my_dict


def dict_to_binary(the_dict):
    string = json.dumps(the_dict)
    binary = ' '.join(format(ord(letter), 'b') for letter in string)
    return binary


new_dict = create_dict(my_url)  # How to write dict in .txt?
print(new_dict)
write_dict = json.dumps(new_dict).encode()
with open('data_currency.txt', 'wb') as file:
    file.write(write_dict)
