import requests


class NBU_REQUEST:

    url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json"
    date = ''
    currency = ''

    def __init__(self, new_date, new_currency):
        self.date = f'&date={new_date}'
        self.currency = f'&valcode={new_currency}'
        self.url = f'{self.url}{self.currency}{self.date}'

    def get_currency_rate(self):
        res = requests.get(self.url)
        j_son_res = res.json()
        my_dict = {}
        for dic in j_son_res:
            for key in dic:
                if key == 'txt':
                    my_dict[dic["txt"]] = dic["rate"]
                    return f'курс {my_dict}'


req1 = NBU_REQUEST(20210505, "USD")
print(req1.url)
print(req1.currency)
print(req1.date)
print(req1.get_currency_rate())





