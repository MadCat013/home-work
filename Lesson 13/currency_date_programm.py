import requests


class NBU_REQUEST:

    url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json"
    date = ''
    currency = ''

    def __init__(self, new_date='', new_currency=''):
        """
        Takes in new parameters from user if they was imputed.
        If not, url = url(current rate of all currencies on the current date)
        :param new_date: YYYYMMDD : STR
        :param new_currency: USD/EUR/ and etc... : STR
        """
        if new_date == '' and new_currency == '':
            pass
        elif type(new_date) != str or type(new_currency) != str:
            raise ValueError(f'\n param : new_date: YYYYMMDD : STR \n param : new_currency: USD/EUR/ and etc... : STR')
        else:
            self.date = f'&date={new_date}'
            self.currency = f'&valcode={new_currency}'
            self.url = f'{self.url}{self.currency}{self.date}'

    def get_currency_rate(self):
        """
        Make request to NBU url, and transforming request.result from Json in dict with data.
        :return: Dict {key = currency : value = currency_rate}
        """
        try:
            res = requests.get(self.url)
        except:
            raise ConnectionError
        if res.content == b'[\r\n{ \r\n"message":"Wrong parameters format"\r\n }\r\n]\r\n':
            raise AttributeError(f"Wrong parameters in {self.currency} or {self.date} : must be in 'YYYYMMDD' format ")
        elif res.content == b'[\r\n]\r\n':
            raise AttributeError(f"Wrong parameters in {self.currency} or {self.date} : must be in 'YYYYMMDD' format ")
        else:
            if 300 > res.status_code >= 200 and res.headers['Content-Type'] == 'application/json; charset=utf-8':
                j_son_res = res.json()
                my_dict = {}
                for dic in j_son_res:
                    my_dict['date'] = dic['exchangedate']
                    for key in dic:
                        if key == 'txt':
                            my_dict[dic["txt"]] = dic["rate"]
                return my_dict
            else:
                raise ConnectionError


def write_dict_in_txt(dic, txt_file_name: str):
    """
    Write dict data in txt document
    :param dic: dict with data
    :param txt_file_name: name for txt file : str
    :return: Nothing, just write data in txt file.
    """
    string = f'Request date : {dic["date"]}'
    dic.pop("date")
    n = 0
    for k, v in dic.items():
        n += 1
        string += f'\n {n}. [{k}] to UAH : [{v}] '
    with open(f'{txt_file_name}.txt', 'a') as file:
        file.write(string)


# 1.задание:
# Подключитесь к API НБУ ( документация тут https://bank.gov.ua/ua/open-data/api-dev ),
# получите текущий курс валют и запишите его в TXT-файл
request_1 = NBU_REQUEST()
write_dict_in_txt(request_1.get_currency_rate(), "test_file")
# 2.задание Пользователь вводит название валюты и дату, программа возвращает пользователю курс гривны к этой валюте
# за указаную дату используя API НБУ. Формат ввода пользователем данных - на ваше усмотрение. Реализовать с помощью ООП!
request_2 = NBU_REQUEST("20211010", "USD")  # При неправильном вводе просто поднимаются ошибки.
print(request_2.get_currency_rate())
