class Vehicle:
    weight = 0
    cost = 0
    manufacturer = ''
    year = 0


class Ship(Vehicle):
    ship_length = 20
    crew = 10

    def __init__(self, new_weight, new_cost, new_manufacturer, new_year):
        self.weight = new_weight
        self.cost = new_cost
        self.manufacturer = new_manufacturer
        self.year = new_year

    def ship_info(self):
        print(f'Manufacturer - "{self.manufacturer}", cost - {self.cost}$, year - {self.year},\n'
              f'weight - {self.weight} kg, length - {self.ship_length}, crew - {self.crew}.')


class Airplane(Vehicle):
    seats = 100
    number_engines = 4

    def __init__(self, new_weight, new_cost, new_manufacturer, new_year):
        self.weight = new_weight
        self.cost = new_cost
        self.manufacturer = new_manufacturer
        self.year = new_year

    def plane_info(self):
        print(f'Manufacturer - "{self.manufacturer}", cost - {self.cost}$, year - {self.year}\n,'
              f'weight - {self.weight} kg, seats - {self.seats}, engines - {self.number_engines}.')


class Car(Vehicle):
    engine_value = 2.0
    transmission = 'auto'

    def __init__(self, new_weight, new_cost, new_manufacturer, new_year):
        self.weight = new_weight
        self.cost = new_cost
        self.manufacturer = new_manufacturer
        self.year = new_year

    def car_info(self):
        print(f'Manufacturer - "{self.manufacturer}", cost - {self.cost}$, year - {self.year}\n,'
              f'weight - {self.weight} kg, engine value - {self.engine_value}, transmission - {self.transmission}.')


ship1 = Ship(20000, 5000000, "Imperial Shipyards", 2021)
ship1.ship_info()

airplane1 = Airplane(40000, 7000000, "Boeing", 2020,)
airplane1.plane_info()

car1 = Car(2500, 30000, "MAZDA", 2021)
car1.car_info()
