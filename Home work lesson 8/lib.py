import random


def get_number(user_number):
    """

    :param user_number: str
    :return: int если прошло проверку на число
    """
    try:
        user_number = int(user_number)
        return my_game(user_number)
    except ValueError:
        return get_number(input("Ведите целое Число!"))


def my_game(user_number):
    """

    :param :  Введеноё число
    :return: если совпало с Х -> True,если нет вызываем функцию "Тепло,холодно"
    """

    x = random.randint(0, 100)
    print(f"Я загадал {x}")

    if x == user_number:
        return True

    elif x != user_number:
        return hot( user_number, x )


def hot(user_number, random_number):
    """

    :param user_number: Число которое ввели
    :param random_number: Число которое загадало
    :return: str(ответ "тепло, холодно")
    """
    res = user_number - random_number

    if res < 0:
        res = -res

    if res >= 10:
        return 'cold'
    elif 10 > res >= 5:
        return 'hot'
    elif res <= 4:
        return 'very hot'



